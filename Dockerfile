FROM centos:7

COPY acceptOTB.exp /data/

ENV \
        UID="1000" \
        GID="1000" \
        UNAME="iota" \
        GNAME="iota" \
        WORKSPACE="/mnt/workspace" \
	IOTABIN="/data/iota2/scripts/Iota2.py"

RUN \
	# install epel-release which is not installed by default on centos docker
        yum install -y usermode epel-release git wget expect bzip2 && \
        cd /data && \
        git clone -b develop https://framagit.org/iota2-project/iota2.git && \
	# bloquer le commit
	git clone https://github.com/ncopa/su-exec && \
        # install and activate dev tools: https://framagit.org/inglada/iota2/issues/48
        yum install -y cmake git gcc python-devel zlib-devel freeglut-devel libX11-devel libXext-devel libXi-devel boost-devel swig gsl gsl-devel python-pip  patch libspatialite-devel libspatialite mpi4py-openmpi mpi4py-common libXrandr-devel libXinerama-devel libXcursor-devel && \
        yum clean all &&  \
	# fix python -c "from mpi4py import MPI" error
	pip install --upgrade pip && \
	pip install  --upgrade setuptools numpy scikit-image pandas matplotlib && \
        yum install -y openmpi-devel && \
        # python deps
        pip install argparse config datetime osr dill && \
        # build tools
        yum install -y centos-release-scl && \
        yum install -y devtoolset-6 && \
        # install pyspatialite deps and pyspatialite
        yum install -y proj-devel geos-devel sqlite-devel && \
        CFLAGS=-I/usr/include pip install pyspatialite && \
        # build cmake from source to get a version > 3.3.0
        yum remove -y cmake && \
        . /opt/rh/devtoolset-6/enable && \
        wget https://cmake.org/files/v3.12/cmake-3.12.1.tar.gz && \
        tar -zxvf cmake-3.12.1.tar.gz && \
        cd cmake-3.12.1 && \
        ./bootstrap --prefix=/usr && \
        make && \
        make install && \
        # build otb
        expect /data/acceptOTB.exp && \
        # test installation
        . /data/iota2/scripts/install/prepare_env.sh && \
        cd /data/iota2/scripts/Tests/UnitTests && \
        python -m unittest Iota2Tests && \
	# create user
	adduser "${UNAME}" && \
	echo "${UNAME} ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers && \
	cd /data/su-exec && \
	make && \
	mv ./su-exec /usr/local/bin && \
        # cleanup
        rm -rf /data/cmake* /data/su-exec /data/acceptOTB.exp && \
        yum clean all && \
        rm -rf /var/cache/yum

COPY entrypoint.sh /usr/local/bin/

VOLUME "${WORKSPACE}"

ENTRYPOINT ["sh", "/usr/local/bin/entrypoint.sh"]